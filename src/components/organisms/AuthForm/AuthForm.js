import React, { useEffect, useRef } from "react";
import "../../../../node_modules/reset.css/reset.css";
import "./styles.css";
import ReactWOW from "react-wow";
import { Register } from "../../../api/register";
import { Login } from "../../../api/login";
//import { Link as link } from "react-router-dom";
//import styled from "styled-components";
import { FacebookProvider, Login as FBLogin } from "react-facebook";
//import { FacebookLoginButton } from "react-social-login-buttons";
import { config } from "../../../config";

function AuthForm(props) {
  const login = useRef(null);
  const loginExist = useRef(null);
  const email = useRef(null);
  const passwordExist = useRef(null);
  const password = useRef(null);

  /* const Link = styled(link)`
    text-decoration: none;
  `;*/
  useEffect(() => {
    const signupButton = document.getElementById("signup-button"),
      loginButton = document.getElementById("login-button"),
      userForms = document.getElementById("user_options-forms");
    let loginStyle = document
      .querySelector(".user_options-unregistered")
      .getAttribute("style");
    let signUpStyle = document
      .querySelector(".user_options-registered")
      .getAttribute("style");
    document
      .querySelector(".user_options-registered")
      .setAttribute("style", "visibility:hidden");
    signupButton.addEventListener(
      "click",
      () => {
        userForms.classList.remove("bounceRight");
        userForms.classList.add("bounceLeft");

        document
          .querySelector(".user_options-registered")
          .setAttribute("style", signUpStyle);

        document
          .querySelector(".user_options-unregistered")
          .setAttribute("style", "visibility:hidden");
      },
      false
    );

    loginButton.addEventListener(
      "click",
      () => {
        userForms.classList.remove("bounceLeft");
        userForms.classList.add("bounceRight");
        document
          .querySelector(".user_options-unregistered")
          .setAttribute("style", loginStyle);

        document
          .querySelector(".user_options-registered")
          .setAttribute("style", "visibility:hidden");
      },
      false
    );
  }, []);

  async function toggleReg(e) {
    e.preventDefault();
    const response = await Register(
      email.current.value,
      login.current.value,
      password.current.value
    );
    if (response.status >= 400) {
      //VSE PLOHO
    } else {
      props.history.push("/additionalinfo");
    }
  }

  async function toggleLogin(e) {
    e.preventDefault();
    const response = await Login(
      loginExist.current.value,
      passwordExist.current.value
    );
    if (response.status >= 400) {
      //VSE PLOHO
    } else {
      sessionStorage.setItem("token", response.token);
      localStorage.setItem("username", response.username);
      props.history.push("/profile");
    }
  }
  const handleResponseReg = async data => {
    let response = await fetch(
      `${
        config.API_ROOT_URL
      }social_auth/convert-token?grant_type=convert_token&client_id=${
        config.FACEBOOK_CLIENT_ID
      }&backend=facebook&token=${data.tokenDetail.accessToken}`,
      {
        method: "POST"
      }
    );
    if (response.status >= 400) {
      //VSE PLOHO
    } else {
      response = await response.json();
      console.log(response);
      sessionStorage.setItem("bearer", response.access_token);
      localStorage.setItem(
        "username",
        `${data.profile.first_name}${data.profile.last_name}`
      );
      props.history.push("/additionalinfo");
    }
  };
  const handleResponse = async data => {
    let response = await fetch(
      `${
        config.API_ROOT_URL
      }social_auth/convert-token?grant_type=convert_token&client_id=${
        config.FACEBOOK_CLIENT_ID
      }&backend=facebook&token=${data.tokenDetail.accessToken}`,
      {
        method: "POST"
      }
    );
    response = await response.json();
    if (response.access_token)
      sessionStorage.setItem("bearer", response.access_token);
    if (data.profile.first_name)
      localStorage.setItem(
        "username",
        `${data.profile.first_name}${data.profile.last_name}`
      );
    props.history.push("/profile");
  };

  const handleError = error => {};
  return (
    <section className="user">
      <ReactWOW animation={"fadeInLeft"}>
        <div className="user_options-container">
          <div className="user_options-text">
            <div className="user_options-unregistered">
              <h2 className="user_unregistered-title">
                Don't have an account?
              </h2>
              <p className="user_unregistered-text">
                You can create it by clicking the "Sign up" button.
              </p>
              <button className="user_unregistered-signup" id="signup-button">
                Sign up
              </button>
            </div>

            <div className="user_options-registered">
              <h2 className="user_registered-title">Have an account?</h2>
              <p className="user_registered-text">
                You can create it by clicking the "Login" button.
              </p>
              <button className="user_registered-login" id="login-button">
                Login
              </button>
            </div>
          </div>

          <div className="user_options-forms" id="user_options-forms">
            <div className="user_forms-login">
              <h2 className="forms_title">Login</h2>
              <form className="forms_form">
                <fieldset className="forms_fieldset">
                  <div className="forms_field">
                    <input
                      type="text"
                      placeholder="Login"
                      className="forms_field-input"
                      ref={loginExist}
                      required
                      autoFocus
                    />
                  </div>
                  <div className="forms_field">
                    <input
                      type="password"
                      placeholder="Password"
                      className="forms_field-input"
                      ref={passwordExist}
                      required
                    />
                  </div>
                </fieldset>
                <div className="forms_buttons">
                  {/*<Link to="/profile" >*/}
                  <input
                    type="submit"
                    value="Log In"
                    className="forms_buttons-action"
                    onClick={toggleLogin}
                  />
                  {/* </Link>*/}
                </div>
                <div className="forms_buttons">
                  {/*<Link to="/profile">*/}
                  <FacebookProvider appId="1047108912142084">
                    <FBLogin
                      scope="email"
                      onCompleted={handleResponse}
                      onError={handleError}
                    >
                      {({ loading, handleClick, error, data }) => (
                        <span onClick={handleClick}>
                          Login via Facebook
                          {loading && <span>Loading...</span>}
                        </span>
                      )}
                    </FBLogin>
                  </FacebookProvider>
                  {/*</Link>*/}
                </div>
              </form>
            </div>
            <div className="user_forms-signup">
              <h2 className="forms_title">Sign Up</h2>
              <form className="forms_form">
                <fieldset className="forms_fieldset">
                  <div className="forms_field">
                    <input
                      type="text"
                      placeholder="Login"
                      className="forms_field-input"
                      ref={login}
                      required
                    />
                  </div>
                  <div className="forms_field">
                    <input
                      type="email"
                      placeholder="Email"
                      className="forms_field-input"
                      ref={email}
                      required
                    />
                  </div>
                  <div className="forms_field">
                    <input
                      type="password"
                      placeholder="Password"
                      className="forms_field-input"
                      ref={password}
                      required
                    />
                  </div>
                </fieldset>
                <div className="forms_buttons">
                  {/*<Link to="/profile" >*/}
                  <input
                    type="submit"
                    value="Sign up"
                    className="forms_buttons-action"
                    onClick={toggleReg}
                  />
                  {/* </Link>*/}
                </div>
                <div className="forms_buttons">
                  {/*<Link to="/profile">*/}
                  <FacebookProvider appId="1047108912142084">
                    <FBLogin
                      scope="email"
                      onCompleted={handleResponseReg}
                      onError={handleError}
                    >
                      {({ loading, handleClick, error, data }) => (
                        <span onClick={handleClick}>
                          Register via Facebook
                          {loading && <span>Loading...</span>}
                        </span>
                      )}
                    </FBLogin>
                  </FacebookProvider>
                </div>
              </form>
            </div>
          </div>
        </div>
      </ReactWOW>
    </section>
  );
}

export default AuthForm;
