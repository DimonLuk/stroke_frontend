import React from "react";
import green from "@material-ui/core/colors/green";
import WelcomeTextAndBtns from "../../molecules/WelcomeTextAndBtns/WelcomeTextAndBtns";
import Text from "../../atoms/Text/Text";
import { Paper } from "@material-ui/core";
import success from "../../../img/success.svg";
import like from "../../../img/like.svg";
import info from "../../../img/info.svg";
import healthcarevector1 from "../../../img/HealthcareVector3.svg";
import pres2 from "../../../img/HealthcareVector1.svg";
import Welcomev2 from "../../molecules/Welcomev2/Welcomev2";
import styled from "styled-components";
import pres1 from "../../../img/pres1_new.png";
import countUp from "react-countup";

function Welcome(props) {
  const Flex = styled.div`
    display: flex;
    justify-content: ${props => props.justify || "center"};
    align-items: ${props => props.alignitems || "center"};
    align-content: ${props => props.aligncontent || "center"};
    flex-wrap: ${props => props.wrap || "nowrap"};
    flex-direction: ${props => props.dir || "row"};
  `;
  const Grid = styled.div`
    display: grid;
    grid-template-columns: repeat(${props => props.cols}, 1fr);
    grid-template-rows: repeat(${props => props.rows}, 1fr);
    justify-content: ${props => props.justify || "center"};
    align-items: ${props => props.alignitems || "center"};
    align-content: ${props => props.aligncontent || "center"};
  `;
  const Gradient = styled.div`
    background: linear-gradient(to bottom, ${green[600]}, ${green[800]});
  `;

  const HealthCareVector1 = styled.img`
    flex-basis: 75%;
    max-width: 583px;
    @media (max-width: 960px) {
      display: none;
    }
  `;
  const TileFlex = styled.div`
    background: white;
    text-align: center;
    flex-basis: 350px;
    z-index: 1000;
    @media (max-width: 1100px) {
      margin: 20px 0px;
    }
  `;
  const TileImg = styled.img`
    max-width: 100px;
    padding: 50px 0px 50px 0px;
  `;
  const TileHeader = styled(Text)`
    padding: 0px 10px 25px 0px;
    color: #343a40;
  `;
  const TileText = styled(Text)`
    padding: 0px 50px 50px 50px;
    font-family: Nunito;
    color: #6c7686;
    height: 225px;
  `;
  const Heading4 = styled(TileHeader)`
    padding: 0px 0px 0px 0px;
  `;
  const Paragraph4 = styled(TileText)`
    fontweight: 100;
    padding: 25px 0px;
  `;
  const CountUp = styled(countUp)`
    color: white;
    font-size: 3em;
    font-family: "Montserrat";
  `;
  const CountHeading = styled(TileHeader)`
    margin: 0;
    padding: 0;
    font-family: "Montserrat";
    color: ${green[500]};
    margin-top: 0.25em;
    font-weight: 500;
  `;

  const PresentationHeading = styled(TileHeader)`
    margin: 0;
    padding: 0;
    font-weight: bold;
  `;
  const TilesFlex = styled(Flex)`
    background: white;
  `;
  const PresentationSection = styled(Grid)`
    background: linear-gradient(80deg, #fafbfe 0, #fafcfe 100%);
    padding: 50px;
  `;
  const FlexCount = styled(Flex)`
    background: #111111;
    box-shadow: 0 2px 10px #111111;
    width: 80%;
    margin: auto;
    @media (max-width: 900px) {
      flex-direction: column;
    }
  `;
  const TextCenter = styled.div`
    text-align: center;
  `;
  const CountElement = styled(TextCenter)`
    padding: 50px 0px;
    text-align: center;
  `;
  const GridPresentation = styled(Grid)`
    display: grid;
    grid-template-areas:
      "Pic1 Txt1"
      "Txt2 Pic2";
    @media (max-width: 1100px) {
      grid-gap: 50px;
      grid-template-columns: 1fr;
      grid-template-rows: 1fr;
      grid-template-areas:
        "Pic1"
        "Txt1"
        "Pic2"
        "Txt2";
    }
  `;
  const GridItem = styled(TextCenter)`
    grid-area: ${props => props.ga};
  `;
  const PresentationDescription = styled(Paragraph4)`
    min-width: 300px;
    max-width: 600px;
    margin: auto;
    fontweight: 100;
  `;

  return (
    <React.Fragment>
      <Gradient center>
        <Flex justify={"center"}>
          <WelcomeTextAndBtns  />
          <HealthCareVector1 src={healthcarevector1}/>
        </Flex>
        <Welcomev2 />
      </Gradient>
      <TilesFlex justify={"space-evenly"} wrap={"wrap"}>
        <TileFlex>
          <Paper className={"hvr-bob"}>
            <TileImg src={success} />
            <TileHeader variant={"h2"} text={"Service"} />
            <TileText
              variant={"body1"}
              text={
                "Service is easy-to-use and multifunctional. Users gain opportunity to recognise not only a probability to have a stroke, it also allows you to know more about your and your relatives health."
              }
            />
          </Paper>
        </TileFlex>
        <TileFlex data-aos="fade-up">
          <Paper className={"hvr-bob"}>
            <TileImg src={like} />
            <TileHeader variant={"h2"} text={"Confidence"} />
            <TileText
              variant={"body1"}
              text={
                "Users decide if it is worth relying on our results. They are calculated by neural network that we developed. It receives the data entered by user and analyses every point to define the probability more precisely."
              }
            />
          </Paper>
        </TileFlex>
        <TileFlex>
          <Paper className={"hvr-bob"}>
            <TileImg src={info} />
            <TileHeader variant={"h2"} text={"Help"} />
            <TileText
              variant={"body1"}
              text={
                "Someone might encounter some minor problems using any program. That`s why we give you an opportunuty to ask for some help and even to offer some improvements for our app. We always try to listen to our clients and help them as we can."
              }
            />
          </Paper>
        </TileFlex>
      </TilesFlex>
      <PresentationSection data-aos="fade-up">
        <TextCenter>
          <Heading4
            variant={"h4"}
            text={"Made for you"}
            style={{ fontWeight: 500, color: green[600], fontFamily: "Nunito" }}
          />
          <PresentationDescription
            variant={"body1"}
            text={
              "We can declare with confidence that our application is one of the best products which meets the users' requirements. It was designed to solve their problems to prevent the bad consequences in the future."
            }
          />
        </TextCenter>
        <GridPresentation rows={2} cols={2}>
          <GridItem ga={"Pic1"}>
            <img
              src={pres1}
              width={"75%"}
              alt={"Mobile application"}
              data-aos="zoom-in"
              data-aos-duration="1000"
              data-aos-anchor-placement="top-center"
            />
          </GridItem>
          <GridItem
            ga={"Txt1"}
            data-aos-duration="1000"
            data-aos="zoom-out"
            data-aos-anchor-placement="top-center"
          >
            <PresentationHeading variant={"h2"} text={"Always available"} />
            <Paragraph4
              variant={"body1"}
              text={
                "The application is versatile. It is categorized with its flexibility and efficiency. Our product is always available on every platform. It is comfortable to use and it doesn't care what platform you're on or what device you're using."
              }
            />
          </GridItem>
          <GridItem
            ga={"Txt2"}
            data-aos-duration="1000"
            data-aos="zoom-out"
            data-aos-anchor-placement="top-center"
          >
            <PresentationHeading variant={"h2"} text={""} />
            <Paragraph4
              data-aos="zoom-out"
              data-aos-duration="1000"
              data-aos-anchor-placement="top-center"
              variant={"body1"}
              text={
                "People don't care about reasons of appearing of the stroke. We solved this problem. One of our computational features is interaction with Telegram Bot , which reminds to maintain health life periodically. Users will receive advices , which will be able to improve their health state."
              }
            />
          </GridItem>
          <GridItem ga={"Pic2"}>
            <img
              src={pres2}
              width={"75%"}
              alt={"Mobile"}
              data-aos="zoom-in"
              data-aos-duration="1000"
              data-aos-anchor-placement="top-center"
            />
          </GridItem>
        </GridPresentation>
      </PresentationSection>
      <FlexCount
        justify={"space-evenly"}
        data-aos="fade-up"
        data-aos-duration="1000"
        data-aos-anchor-placement="top-center"
      >
        <CountElement>
          <CountUp start={0} end={95} duration={3} suffix={"%"} separator=" " />
          <CountHeading variant={"h2"} text={"Satisfied users"} />
        </CountElement>
        <CountElement>
          <CountUp start={0} end={300} duration={3} separator=" " />
          <CountHeading variant={"h2"} text={"Helped today"} />
        </CountElement>
        <CountElement>
          <CountUp
            start={0}
            end={100000}
            duration={3}
            separator=" "
            suffix={""}
          />
          <CountHeading variant={"h2"} text={"Total users"} />
        </CountElement>
      </FlexCount>
    </React.Fragment>
  );
}

export default Welcome;
