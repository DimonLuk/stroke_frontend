import React, { useState, useContext } from "react";
import styled from "styled-components";
import green from "@material-ui/core/colors/green";
import { Link as link } from "react-router-dom";
import btn from "../../atoms/Button/Button";
import { config } from "../../../config";
import { IsLoggedContext } from "../../atoms/IsLoggedContext/IsLoggedContext";

import { token as genToken } from "../../../api/tokenCheck";
const StrokeTest = props => {
  const TestContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 75%;
    margin: 75px auto 0px;
    text-align: center;
    justify-content: space-between;
    font-family: "Montserrat", sans-serif;
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.35);
  `;
  const Link = styled(link)`
    text-decoration: none;
  `;
  const TestHeader = styled.h1`
    font-size: 35px;
    margin: 50px auto;
    font-weight: 700;
    color: ${green[600]};
    line-height: 1.5;
  `;
  const TestBtns = styled.div`
    display: flex;
    justify-content: space-evenly;
    align-items: center;
    align-content: center;

    @media (max-width: 900px) {
      flex-direction: column;
    }
  `;
  const Button = styled(btn)`
    background: ${green[500]};
    min-height: 50px;
    width: 20%;
    min-width: 100px;
    margin: 0px 0px 50px;
    color: white;
    font-size: 1rem;

    &:hover {
      background: ${green[200]};
    }
    @media (max-width: 900px) {
      width: 75%;
      margin: none;
    }
  `;
  const StrokeSignsSmallText = styled.p`
    margin: 0px auto 50px;
    font-size: 20px;
    line-height: 40px;
    font-family: "Montserrat", sans-serif;
    width: 70%;
    font-weight: 500;
    color: #333333;
  `;
  /*const LinkButton = styled(Link)`
    height: 50px;
    min-width: 100px;
    &:hover {
      background: ${green[100]};
    }
    transition: background 0.3s;
    border-radius: 3px;
  `;
  const SkipBtns = styled.div`
    display: flex;
    width: 90%;
    justify-content: space-evenly;
  `;*/
  const [questionNumber, setQuestionNumber] = useState(0);
  const [answers, setAnswers] = useState([]);

  const questions = [
    {
      text: "How old are you?",
      answers: ["Under 45", "Over 45"],
      values: [0, 1]
    },
    {
      text: "What is your sex?",
      answers: ["Male", "Female"],
      values: [1, 0]
    },
    {
      text: "What is value of your arterial pressure?",
      answers: ["Under 130 mm Hg", "Over 130 mm Hg"],
      values: [0, 1]
    },
    {
      text: "Did you have hyperextension?",
      answers: ["Yes", "No"],
      values: [0, 1]
    },
    {
      text: "Do you smoke?",
      answers: ["No", "Yes"],
      values: [0, 1]
    },
    {
      text: "What is your serum cholesterol level?",
      answers: ["160-230 mg / dl", "Over 230 mg / dl"],
      values: [0, 1]
    },
    {
      text: "What is value of your blood sugar?",
      answers: [
        "Under 6.6 millimole per liter",
        "Over 6.6 millimole per liter"
      ],
      values: [0, 1]
    },
    {
      text: "Did your relatives have coronary heart disease?",
      answers: ["Yes", "No"],
      values: [0, 1]
    },
    {
      text: "Are you overweight?",
      answers: ["My weight is normal", "I am overweight"],
      values: [0, 1]
    },
    {
      text: "Do you have diabet?",
      answers: ["Yes", "No"],
      values: [0, 1]
    },
    {
      text: "What is your heart rate?",
      answers: ["Between 60 and 100 BPM", "More than 100 BPM"],
      values: [0, 1]
    },
    {
      text: "Do you have stress at home or during your working?",
      answers: ["Yes", "No"],
      values: [0, 1]
    },
    {
      text: "Do your vessels have atherosclerotic plaques?",
      answers: ["Yes", "No"],
      values: [0, 1]
    },
    {
      text: "Do you have chest pain?",
      answers: ["Yes", "No"],
      values: [0, 1]
    },
    {
      text: "Do you have many large vessels?",
      answers: ["Yes", "No"],
      values: [0, 1]
    },
    {
      text: "Do you drink alcohol more than once a week?",
      answers: ["Yes", "No"],
      values: [0, 1]
    },
    {
      text: "Has your memory deteriorated?",
      answers: ["Yes", "No"],
      values: [0, 1]
    },
    {
      text: "Did the doctor tell you that your heart size has increased?",
      answers: ["Yes", "No"],
      values: [0, 1]
    },
    {
      text: "Do you have dementia(dotage)?",
      answers: ["Yes", "No"],
      values: [0, 1]
    },
    {
      text: "Do you have sport exercises every week?",
      answers: ["Yes", "No"],
      values: [0, 1]
    }
  ];
  const setResult = useContext(IsLoggedContext)[1];

  function ConditionalRedirect({ questionNumber, value, index }) {
    if (questionNumber === 19) {
      return (
        <Link to="/result" style={{ width: "20%" }}>
          <Button
            style={{ width: "100%" }}
            key={index}
            text={value}
            onClick={e => {
              if (questionNumber < 19) {
                setQuestionNumber(prevstate => {
                  if (prevstate < 19) return prevstate + 1;
                  else return prevstate;
                });
                setAnswers(answers => {
                  answers.push(questions[questionNumber].values[index]);
                  return answers;
                });
              } else {
                const sendReq = async () => {
                  let res = await fetch(`${config.API_ROOT_URL}predict/`, {
                    method: "POST",
                    headers: {
                      Accept: "application/json",
                      "Content-Type": "application/json",
                      Authorization: genToken()
                    },
                    body: JSON.stringify({
                      answers
                    })
                  });
                  res = await res.json();
                  setResult(prev => (prev.result = res));
                  console.log(res);
                  //TODO: Redirect to result page with res prop
                };
                sendReq();
              }
            }}
          />
        </Link>
      );
    } else {
      return (
        <Button
          key={index}
          text={value}
          onClick={e => {
            if (questionNumber < 19) {
              setQuestionNumber(prevstate => {
                if (prevstate < 19) return prevstate + 1;
                else return prevstate;
              });
              setAnswers(answers => {
                answers.push(questions[questionNumber].values[index]);
                return answers;
              });
            } else {
              const sendReq = async () => {
                let res = await fetch(`${config.API_ROOT_URL}predict/`, {
                  method: "POST",
                  headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: genToken()
                  },
                  body: JSON.stringify({
                    answers
                  })
                });
                res = await res.json();
                console.log(res);
                //TODO: Redirect to result page with res prop
              };
              sendReq();
            }
          }}
        />
      );
    }
  }
  return (
    <TestContainer>
      <TestHeader>Question {questionNumber + 1}</TestHeader>
      <StrokeSignsSmallText>
        {questions[questionNumber].text}
      </StrokeSignsSmallText>
      <TestBtns>
        {questions[questionNumber].answers.map((value, index) => (
          <ConditionalRedirect
            value={value}
            index={index}
            questionNumber={questionNumber}
            key={index}
          />
        ))}
      </TestBtns>
      {/*<SkipBtns>
        <LinkButton>
          <LinkInner
            text="Previous"
            onClick={() => {
              setQuestionNumber(prevstate => {
                debugger;
                if (prevstate > 0) return prevstate - 1;
                else return prevstate;
              });
            }}
          />
        </LinkButton>
        <LinkButton>
          <LinkInner
            text="Next"
            onClick={() => {
              setAnswers(answers => answers.push(questions[questionNumber].value[]))
              setQuestionNumber(prevstate => {
                if (prevstate < 19) return prevstate + 1;
                else return prevstate;
              });
            }}
          />
        </LinkButton>
      </SkipBtns>
          */}
    </TestContainer>
  );
};

export default StrokeTest;
