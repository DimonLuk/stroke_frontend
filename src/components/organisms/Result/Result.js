import React, { useContext } from "react";
import styled from "styled-components";
import green from "@material-ui/core/colors/green";
import red from "@material-ui/core/colors/red";
import yellow from "@material-ui/core/colors/yellow";
import Nurse from "../../../img/Vector_Nurse_in_Hospital.svg";
import { IsLoggedContext } from "../../atoms/IsLoggedContext/IsLoggedContext";

const Result = props => {
  const TestContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 90%;
    max-width: 1000px;
    margin: 75px auto 0px;
    font-family: "Montserrat", sans-serif;
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.35);
    overflow: hidden;
  `;
  const TestHeader = styled.h1`
    font-size: 45px;
    margin: 75px 25px 0px 50px;
    font-weight: 700;
    color: ${green[600]};
    @media (max-width: 555px) {
      font-size: 35px;
      margin: 50px 25px 0px;
    }
    @media (max-width: 400px) {
      font-size: 30px;
    }
  `;
  const SmallTextContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
    height: 100%;
    margin: 25px 0px 50px;
  `;
  const StrokeSignsSmallText = styled.p`
    font-size: 30px;
    line-height: 1.5;
    font-family: "Montserrat", sans-serif;
    margin: 50px 25px 0px 50px;
    width: 70%;
    font-weight: 700;
    color: #333333;
    @media (max-width: 555px) {
      font-size: 20px;
      margin: 10px 25px;
    }
  `;
  const InnerContainer = styled.div`
    display: flex;
    overflow: hidden;
  `;
  const VectorNurse = styled.img`
    position:relative;
    top:-125px
    max-height: 550px;
    @media (max-width: 1060px) {
      display:none;
    }
  `;
  const BigLetter = styled.span`
    color: ${props =>
      props.color === "yellow"
        ? yellow[700]
        : props.color === "red"
        ? red[500]
        : green[500]};
    font-size: 30px;
    font-weight: 800;
    font-family: "Montserrat", sans-serif;
    @media (max-width: 555px) {
      font-size: 20px;
    }
  `;
  const result = useContext(IsLoggedContext)[0].result;

  return (
    <TestContainer>
      <TestHeader>Congratulations!</TestHeader>
      <InnerContainer>
        <SmallTextContainer>
          <StrokeSignsSmallText>
            You have succesfully passed our test.
          </StrokeSignsSmallText>
          <StrokeSignsSmallText>
            Your probability of insult is
            <BigLetter
              color={
                parseInt(result) > 5
                  ? "red"
                  : parseInt(result) > 1
                  ? "yellow"
                  : "green"
              }
            >
              {" " + result + "%"}
            </BigLetter>
          </StrokeSignsSmallText>
          {parseInt(result) > 5 ? (
            <StrokeSignsSmallText>
              That means that you might want to{" "}
              <BigLetter color={"red"}>consider going to doctor.</BigLetter>
            </StrokeSignsSmallText>
          ) : parseInt(result) < 1 ? (
            <StrokeSignsSmallText>
              That means you should have
              <BigLetter color={"green"}> nothing to worry about.</BigLetter>
            </StrokeSignsSmallText>
          ) : (
            <StrokeSignsSmallText>
              That means that you might want to consider
              <BigLetter color={"yellow"}> changing your lifestyle.</BigLetter>
            </StrokeSignsSmallText>
          )}
        </SmallTextContainer>
        <VectorNurse src={Nurse} />
      </InnerContainer>
    </TestContainer>
  );
};

export default Result;
