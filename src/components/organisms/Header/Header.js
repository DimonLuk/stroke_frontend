import React, { useState } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import MenuDrawer from "../../molecules/MenuDrawer/MenuDrawer";
import Logo from "../../molecules/Logo/Logo";
import Auth from "../../molecules/Auth/Auth";

const styles = theme => ({
  list: {
    width: 250
  },
  appbar: {
    boxShadow: "none",
    top: 0,
    position: "fixed",
    maxWidth: 1920,
    left: "50%",
    transform: "translateX(-50%)"
  },
  appbarfiller: {
    margin: "auto",
    maxWidth: 1920
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
    cursor: "pointer"
  },
  logo: {
    marginRight: 10
  },
  auth: {
    position: "absolute",
    right: "10px",
    zIndex: 100
  }
});

const Header = props => {
  const initState = {
    anchorEl: null,
    left: false
  };
  const [state, setState] = useState(initState);
  const toggleDrawer = open => () => {
    setState({ left: open });
  };

  const handleChange = event => {
    setState({ auth: event.target.checked });
  };

  const handleMenu = event => {
    setState({ anchorEl: event.currentTarget });
  };

  const handleClose = () => {
    setState({ anchorEl: null });
  };
  const { classes } = props;
  const { anchorEl, left } = state;
  const open = Boolean(anchorEl);
  return (
    <div>
      <AppBar className={classes.appbarfiller} position="static">
        <Toolbar />
      </AppBar>
      <AppBar className={classes.appbar}>
        <Toolbar>
          <IconButton
            onClick={toggleDrawer(true)}
            className={classes.menuButton}
            color="inherit"
            aria-label="Menu"
          >
            <MenuIcon />
          </IconButton>
          <MenuDrawer
            className={classes.menuDrawer}
            toggleDrawer={toggleDrawer}
            left={left}
            style={{ position: "absolute", right: 0 }}
          />
          <Logo />
          {localStorage.username && (
            <Auth
              className={classes.auth}
              anchorEl={anchorEl}
              open={open}
              handleChange={handleChange}
              handleClose={handleClose}
              handleMenu={handleMenu}
            />
          )}
        </Toolbar>
      </AppBar>
    </div>
  );
};

Header.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Header);
