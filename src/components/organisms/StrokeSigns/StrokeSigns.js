import React from "react";
import "hover.css";
import "animate.css";
import green from "@material-ui/core/colors/green";
import styled from "styled-components";

function StrokeSigns() {
  const StrokeSignsContainer = styled.div`
    width: 90%;
    margin: 100px auto 0px;
  `;
  const StrokeSignsHeader = styled.h1`
    margin: 100px 0px 0px 100px;
    font-size: 60px;
    font-weight: 700;
    font-family: "Montserrat", sans-serif;
    color: #333333;
    @media (max-width: 660px) {
      margin: 0px 0px 0px 0px;

      font-size: 50px;
    }

    @media (max-width: 500px) {
      margin: 0px 0px 0px 0px;

      font-size: 45px;
    }

    @media (max-width: 400px) {
      font-size: 40px;
    }
  `;
  const StrokeSignsBigText = styled.p`
    margin: 100px 0px;
    font-size: 40px;
    line-height: 50px;
    font-family: "Montserrat", sans-serif;
    width: 85%;
    font-weight: 500;
    color: #333333;
    @media (max-width: 960px) {
      font-size: 35px;
      line-height: 45px;
    }
    @media (max-width: 660px) {
      font-size: 27px;
      line-height: 40px;
    }

    @media (max-width: 500px) {
      font-size: 22px;
      line-height: 35px;
    }

    @media (max-width: 450px) {
      font-size: 20px;
      line-height: 30px;
    }
  `;
  const StrokeSignsSmallText = styled.p`
    margin: 0px 0px;
    font-size: 20px;
    line-height: 40px;
    font-family: "Montserrat", sans-serif;
    width: 60%;
    font-weight: 500;
    color: #333333;
    @media (max-width: 960px) {
      font-size: 18px;
      line-height: 35px;
    }

    @media (max-width: 660px) {
      font-size: 16px;
      line-height: 30px;
      width: 100%;
    }
  `;
  const BigLetter = styled.span`
    color: ${green[500]};
    font-size: 30px;
    font-weight: 800;
    font-family: "Montserrat", sans-serif;

    @media (max-width: 660px) {
      font-size: 25px;
    }
  `;

  const BoldBig = styled.span`
    color: #333333;

    font-size: 25px;
    font-weight: 800;
    font-family: "Montserrat", sans-serif;

    @media (max-width: 660px) {
      font-size: 20px;
    }
  `;
  return (
    <React.Fragment>
      <StrokeSignsContainer>
        <StrokeSignsHeader>Stroke Signs</StrokeSignsHeader>
        <StrokeSignsBigText>
          When you have a stroke, your brain isn't getting the blood it needs.
          You need treatment right away to lower your chances of brain damage,
          disability, or even death.
        </StrokeSignsBigText>
        <StrokeSignsSmallText>
          Use the
          <BoldBig> FAST </BoldBig>
          test to check for the most common symptoms of a stroke in yourself or
          someone else.
        </StrokeSignsSmallText>
        <StrokeSignsSmallText>
          <BigLetter>F</BigLetter>ace: Smile and see if one side of the face
          droops.
        </StrokeSignsSmallText>
        <StrokeSignsSmallText>
          <BigLetter>A</BigLetter>rms: Raise both arms. Does one arm drop down?
        </StrokeSignsSmallText>
        <StrokeSignsSmallText>
          <BigLetter>S</BigLetter>peech: Say a short phrase and check for
          slurred or strange speech.
        </StrokeSignsSmallText>
        <StrokeSignsSmallText>
          <BigLetter>T</BigLetter>ime: If the answer to any of these is yes,
          call 911 right away and write down the time when symptoms started.
        </StrokeSignsSmallText>
        <StrokeSignsSmallText>
          Minutes matter in treating stroke.
        </StrokeSignsSmallText>
        <StrokeSignsSmallText>
          Calling a doctor or driving to the hospital yourself wastes time.
          Ambulance workers can judge your situation sooner, and that boosts
          your chance of getting the treatment you need as soon as possible.
        </StrokeSignsSmallText>
      </StrokeSignsContainer>
    </React.Fragment>
  );
}

export default StrokeSigns;
