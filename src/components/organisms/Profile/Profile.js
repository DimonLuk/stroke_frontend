import React, { useEffect, useState } from "react";
import styled from "styled-components";
import green from "@material-ui/core/colors/green";
import Button from "../../atoms/Button/Button";
import { config } from "../../../config";
import AccountCircle from "@material-ui/icons/AccountCircle";
import { profile } from "../../../api/profile";
import select from "react-select";
import { Form, Field } from "react-final-form";
import { token as getToken } from "../../../api/tokenCheck";
const Profile = props => {
  const Container = styled.div`
    display: flex;
    width: 90%;
    margin: 50px auto 0px;
    justify-content: space-between;
    font-family: "Montserrat", sans-serif;
  `;
  const AdditionalInfo = styled.div`
    display: flex;
    flex-direction: column;
    flex-basis: 80%;
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.35);
    margin: auto;
  `;
  const AdditionalInfoHeader = styled.h1`
    font-size: 35px;
    font-weight: 700;
    color: ${green[600]};
    align-self: center;
    margin-left: 15px;
    line-height: 1.5;
  `;
  const InputLabel = styled.label`
    font-family: "Montserrat", sans-serif;
    font-weight: 600;
    font-size: 0.8rem;
    color: #111111;
    line-height: 1.2;
  `;
  const InputInfo = styled.input`
    margin-top: 20px;
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.35);
    height: 40px;
    width: 100%;
  `;
  const InputAndLabel = styled.div`
    display: flex;
    flex-direction: column;
    width: 25%;
  `;
  const InputAndLabelRow = styled.div`
    display: flex;
    width: 100%;
    margin: 25px auto;
    justify-content: space-around;
  `;
  const InputContainer = styled.div`
    display: flex;
    margin: 25px 0 0 25px;
    flex-direction: column;
    height: 75%;
    width: 90%;
    justify-content: space-around;
  `;
  const GreenButton = styled(Button)`
    background: ${green[500]};
    height: 40px;
    color: white;
    font-size: 1rem;
    margin: 35px 0px 50px;

    &:hover {
      background: ${green[200]};
    }
  `;
  const HeaderContainer = styled.div`
    display: flex;
    margin: 25px 0px 0px 50px;
  `;

  const Select = styled(select)`
    position: absolute;
    bottom: 0;
  `;
  const FieldInputInfo = props => (
    <Field name={props.name}>
      {({ input, meta }) => (
        <div>
          <InputInfo {...input} {...props} />
          {meta.touched && meta.error && <span>{meta.error}</span>}
        </div>
      )}
    </Field>
  );
  const [initialValues, setInitialiValues] = useState({});
  const [accountOptions, setAccountOptions] = useState([]);
  const [countryOptions, setCountryOptions] = useState([]);

  useEffect(() => {
    async function fetchData() {
      try {
        const data = await profile();
        data.country = { value: data.country, label: data.country };
        data.sex = { value: data.sex, label: data.sex };
        setInitialiValues(data);
      } catch (ex) {}
      let res = await fetch(`${config.API_ROOT_URL}user_info/accounts_list/`, {
        headers: {
          Authorization: getToken()
        }
      });
      res = await res.json();

      let counts = await fetch(
        `${config.API_ROOT_URL}user_info/countries_list/`,
        {
          headers: {
            Authorization: getToken()
          }
        }
      );
      counts = await counts.json();
      counts = counts.map(v => ({ label: v, value: v }));
      setAccountOptions(res);
      setCountryOptions(counts);
    }

    fetchData();
  }, []);

  const onSubmit = async values => {
    const toSend = { ...values };
    if (toSend.country.value) toSend.country = toSend.country.value;
    if (toSend.sex.value) toSend.sex = toSend.sex.value;
    await fetch(
      `${config.API_ROOT_URL}registration/${localStorage.getItem("username")}/`,
      {
        method: "PATCH",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: getToken()
        },
        body: JSON.stringify(toSend)
      }
    );
    await fetch(
      `${config.API_ROOT_URL}user_info/${localStorage.getItem("username")}/`,
      {
        method: "PATCH",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: getToken()
        },
        body: JSON.stringify(toSend)
      }
    );
  };

  return (
    <Form
      onSubmit={onSubmit}
      initialValues={initialValues}
      render={({ handleSubmit, pristine, invalid }) => (
        <form onSubmit={handleSubmit}>
          <Container>
            <AdditionalInfo>
              <HeaderContainer>
                <AccountCircle
                  style={{
                    fontSize: "75px",
                    color: green[500],
                    filter: "drop-shadow(0px 2px 5px rgba(0,0,0,0.35))"
                  }}
                />
                <AdditionalInfoHeader>Your account</AdditionalInfoHeader>
              </HeaderContainer>
              <InputContainer>
                <InputAndLabelRow>
                  <InputAndLabel>
                    <InputLabel>Name</InputLabel>
                    <FieldInputInfo name="first_name" required />
                  </InputAndLabel>
                  <InputAndLabel>
                    <InputLabel>Age</InputLabel>
                    <FieldInputInfo type="number" name="age" required />
                  </InputAndLabel>
                  <InputAndLabel>
                    <InputLabel>Mail</InputLabel>
                    <FieldInputInfo name="email" required />
                  </InputAndLabel>
                </InputAndLabelRow>
                <InputAndLabelRow>
                  <InputAndLabel>
                    <InputLabel>Sex</InputLabel>
                    <Field
                      name="sex"
                      options={[
                        { value: "Male", label: "Male" },
                        { value: "Female", label: "Female" }
                      ]}
                    >
                      {({ input, ...rest }) => <Select {...input} {...rest} />}
                    </Field>
                  </InputAndLabel>
                  <InputAndLabel>
                    <InputLabel>Country</InputLabel>
                    <Field name="country" options={countryOptions}>
                      {({ input, ...rest }) => <Select {...input} {...rest} />}
                    </Field>
                  </InputAndLabel>
                  <InputAndLabel>
                    <InputLabel>Linked accounts</InputLabel>
                    <Field name="linked_accounts" options={accountOptions}>
                      {({ input, ...rest }) => (
                        <Select
                          {...input}
                          {...rest}
                          onChange={(selected, removedValue) => {
                            if (selected && input.value) {
                              input.value.push(selected);
                            } else if (input.value) {
                              input.value = input.value.filter(
                                item =>
                                  item.value !== removedValue.removedValue.value
                              );
                            } else if (selected && !input.value) {
                              input.value = [selected];
                            }
                            input.onChange(selected, removedValue);
                          }}
                          isMulti
                        />
                      )}
                    </Field>
                  </InputAndLabel>
                </InputAndLabelRow>
                <InputAndLabelRow>
                  <InputAndLabel>
                    <InputLabel>Telegram</InputLabel>
                    <FieldInputInfo name="telegram_account" />
                  </InputAndLabel>
                  <InputAndLabel>
                    <GreenButton type="submit" text="CHANGE" />
                  </InputAndLabel>
                </InputAndLabelRow>
              </InputContainer>
            </AdditionalInfo>
          </Container>
        </form>
      )}
    />
  );
};

export default Profile;
