import React, { useState, useEffect } from "react";
import styled from "styled-components";
import green from "@material-ui/core/colors/green";
import syringe from "../../../img/Syringe.svg";
import Button from "../../atoms/Button/Button";
import { Form, Field } from "react-final-form";
import { config } from "../../../config";
import { token as genToken } from "../../../api/tokenCheck";
const Coagulogram = props => {
  const Container = styled.div`
    display: flex;
    width: 90%;
    margin: 100px auto 0px;
    justify-content: space-between;
    font-family: "Montserrat", sans-serif;
  `;
  const AdditionalInfo = styled.div`
    display: flex;
    flex-direction: column;
    flex-basis: 60%;
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.35);
    margin: 0px auto;
    @media (max-width: 1000px) {
      flex-basis: 100%;
    }
  `;
  const VectorImg = styled.div`
    flex-basis: 30%;
    @media (max-width: 1000px) {
      display: none;
    }
  `;
  const AdditionalInfoHeader = styled.h1`
    font-size: 35px;
    margin: 50px 25px 0px 50px;
    font-weight: 700;
    color: ${green[600]};
    line-height: 1.5;

    @media (max-width: 450px) {
      font-size: 25px;
      margin: 25px 0px 0px 25px;
    }
  `;
  const InputLabel = styled.label`
    font-family: "Montserrat", sans-serif;
    font-weight: 600;
    font-size: 1rem;
    color: #111111;
    line-height: 1.5;
    @media (max-width: 800px) {
      font-size: 0.8rem;
    }
    @media (max-width: 640px) {
      font-size: 0.65rem;
    }
  `;
  const InputInfo = styled.input`
    margin-top: 20px;
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.35);
    height: 50px;
    width: 100%;
    @media (max-width: 800px) {
      height: 45px;
    }
    @media (max-width: 640px) {
      height: 40px;
    }
  `;
  const InputAndLabel = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
    width: 35%;
    justify-content: center;

    @media (max-width: 640px) {
      width: 90%;
    }
  `;
  const InputAndLabelRow = styled.div`
    display: flex;
    width: 100%;
    height: 50%;
    margin: auto;
    justify-content: space-around;

    @media (max-width: 640px) {
      flex-direction: column;
    }
  `;
  const InputContainer = styled.div`
    display: flex;
    margin: 25px 0 0 25px;
    flex-direction: column;
    height: 60%;
    width: 90%;
    justify-content: space-around;
  `;
  const GreenButton = styled(Button)`
    background: ${green[500]};
    height: 50px;
    width: 32%;
    margin: 10px 13% 10px auto;
    color: white;
    font-size: 1rem;
    &:hover {
      background: ${green[200]};
    }

    @media (max-width: 640px) {
      margin: 20px auto;
    }
  `;
  const FieldInputInfo = props => (
    <Field name={props.name}>
      {({ input, meta }) => (
        <div>
          <InputInfo {...input} {...props} />
          {meta.touched && meta.error && <span>{meta.error}</span>}
        </div>
      )}
    </Field>
  );

  const [initialValues, setInitialValues] = useState({});
  const onSubmit = async values => {
    const toSend = { ...values };
    requestType === "POST"
      ? fetch(`${config.API_ROOT_URL}analysis/`, {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: genToken()
          },
          body: JSON.stringify(toSend)
        })
      : fetch(`${config.API_ROOT_URL}analysis/${localStorage.username}/`, {
          method: "PATCH",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: genToken()
          },
          body: JSON.stringify(toSend)
        });
  };
  const [requestType, setRequestType] = useState("PATCH");
  useEffect(() => {
    const fetchData = async () => {
      try {
        let res = await fetch(
          `${config.API_ROOT_URL}analysis/${localStorage.username}/`,
          {
            headers: {
              Authorization: genToken()
            }
          }
        );
        if (res.status >= 400) {
          throw Error;
        }
        res = await res.json();
        setInitialValues(res);
      } catch (e) {
        setRequestType("POST");
      }
    };
    fetchData();
  }, []);

  return (
    <Form
      onSubmit={onSubmit}
      initialValues={initialValues}
      render={({ handleSubmit, pristine, invalid }) => (
        <form onSubmit={handleSubmit}>
          <Container>
            <AdditionalInfo>
              <AdditionalInfoHeader>
                Please enter your analysis results
              </AdditionalInfoHeader>
              <InputContainer>
                <InputAndLabelRow>
                  <InputAndLabel>
                    <InputLabel>Prothrombin index</InputLabel>
                    <FieldInputInfo
                      type="number"
                      min="40"
                      max="80"
                      name="prothrombin_index"
                    />
                  </InputAndLabel>
                  <InputAndLabel>
                    <InputLabel>Blood clotting time</InputLabel>
                    <FieldInputInfo
                      type="number"
                      min="40"
                      max="80"
                      name="blood_clotting"
                    />
                  </InputAndLabel>
                </InputAndLabelRow>
                <InputAndLabelRow>
                  <InputAndLabel>
                    <InputLabel>Maximum permissible limit</InputLabel>
                    <FieldInputInfo
                      type="number"
                      min="40"
                      max="80"
                      name="maximum_permissible_limit"
                    />
                  </InputAndLabel>
                  <InputAndLabel>
                    <InputLabel>Date of analysis </InputLabel>
                    <FieldInputInfo type="date" name="date_of_analysis" />
                  </InputAndLabel>
                </InputAndLabelRow>
              </InputContainer>
              <GreenButton type="submit" text="FINISH" />
            </AdditionalInfo>
            <VectorImg>
              <img src={syringe} alt="Syringe" height={"450"} />
            </VectorImg>
          </Container>
        </form>
      )}
    />
  );
};

export default Coagulogram;
