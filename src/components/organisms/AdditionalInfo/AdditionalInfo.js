import React, { useState, useEffect } from "react";
import styled from "styled-components";
import green from "@material-ui/core/colors/green";
import Button from "../../atoms/Button/Button";
import { config } from "../../../config";
import { Form, Field } from "react-final-form";
import select from "react-select";
import { token as genToken } from "../../../api/tokenCheck";
const Profile = props => {
  const Container = styled.div`
    display: flex;
    width: 90%;
    margin: 50px auto 0px;
    justify-content: space-between;
    font-family: "Montserrat", sans-serif;
  `;
  const AdditionalInfo = styled.div`
    display: flex;
    flex-direction: column;
    flex-basis: 80%;
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.35);
    margin: auto;
  `;
  const AdditionalInfoHeader = styled.h1`
    font-size: 35px;
    margin: 50px 25px 0px 50px;
    font-weight: 700;
    color: ${green[600]};
    line-height: 1.5;
  `;
  const InputLabel = styled.label`
    display: flex;
    font-family: "Montserrat", sans-serif;
    font-weight: 600;
    font-size: 0.8rem;
    color: #111111;
    line-height: 1.2;
    margin: auto 0px;
  `;
  const InputInfo = styled.input`
    display:flex
    margin-top: 20px;
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.35);
    height: 40px;
    width: 100%;
  `;
  const InputAndLabel = styled.div`
    display: flex;
    flex-direction: column;
    width: 25%;
    justify-content: flex-end;
  `;
  const InputAndLabelRow = styled.div`
    display: flex;
    width: 100%;
    margin: 0px auto 25px;
    justify-content: space-around;
  `;
  const InputContainer = styled.div`
    display: flex;
    margin: 50px 0 50px 25px;
    flex-direction: column;
    height: 75%;
    width: 90%;
    justify-content: space-around;
  `;
  const GreenButton = styled(Button)`
    background: ${green[500]};
    height: 40px;
    color: white;
    font-size: 1rem;
    width: 25%;
    margin: 0 0 0 auto;
    &:hover {
      background: ${green[200]};
    }
  `;
  const Select = styled(select)`
    margin-top: 20px;
  `;
  const FieldInputInfo = props => (
    <Field name={props.name}>
      {({ input, meta }) => (
        <div>
          <InputInfo {...input} {...props} />
          {meta.touched && meta.error && <span>{meta.error}</span>}
        </div>
      )}
    </Field>
  );
  const [accountOptions, setAccountOptions] = useState([]);
  const [countryOptions, setCountryOptions] = useState([]);
  const [initialValues] = useState({});
  useEffect(() => {
    async function fetchData() {
      let counts = await fetch(
        `${config.API_ROOT_URL}user_info/countries_list/`,
        {
          headers: {
            Authorization: genToken()
          }
        }
      );
      let res = await fetch(`${config.API_ROOT_URL}user_info/accounts_list/`, {
        headers: { Authorization: genToken() }
      });
      if (res.status < 400) {
        res = await res.json();

        setAccountOptions(res);
      }
      if (counts.status < 400) {
        counts = await counts.json();
        counts = counts.map(v => ({ label: v, value: v }));
        setCountryOptions(counts);
      }
    }
    fetchData();
  },[]);
  const onSubmit = async values => {
    const toSend = { ...values };
    if (toSend.country.value) {
      toSend.country = toSend.country.value;
    }
    if (toSend.sex.value) {
      toSend.sex = toSend.sex.value;
    }
    await fetch(
      `${config.API_ROOT_URL}registration/${localStorage.getItem("username")}/`,
      {
        method: "PATCH",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: genToken()
        },
        body: JSON.stringify(toSend)
      }
    );
    await fetch(`${config.API_ROOT_URL}user_info/`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: genToken()
      },
      body: JSON.stringify(toSend)
    });
  };
  return (
    <Form
      onSubmit={onSubmit}
      initialValues={initialValues}
      render={({ handleSubmit, pristine, invalid }) => (
        <form onSubmit={handleSubmit}>
          <Container>
            <AdditionalInfo>
              <AdditionalInfoHeader>
                Additional Information
              </AdditionalInfoHeader>
              <InputContainer>
                <InputAndLabelRow>
                  <InputAndLabel>
                    <InputLabel>Name</InputLabel>
                    <FieldInputInfo name="first_name" />
                  </InputAndLabel>
                  <InputAndLabel>
                    <InputLabel>Age</InputLabel>
                    <FieldInputInfo
                      type="number"
                      min="0"
                      max="120"
                      name="age"
                    />
                  </InputAndLabel>
                  <InputAndLabel>
                    <InputLabel>Telegram</InputLabel>
                    <FieldInputInfo name="telegram_account" />
                  </InputAndLabel>
                </InputAndLabelRow>
                <InputAndLabelRow>
                  <InputAndLabel>
                    <InputLabel>Sex</InputLabel>
                    <Field
                      name="sex"
                      options={[
                        { value: "Male", label: "Male" },
                        { value: "Female", label: "Female" }
                      ]}
                    >
                      {({ input, ...rest }) => <Select {...input} {...rest} />}
                    </Field>
                  </InputAndLabel>
                  <InputAndLabel>
                    <InputLabel>Country</InputLabel>
                    <Field name="country" options={countryOptions}>
                      {({ input, ...rest }) => (
                        <div>
                          <Select {...input} {...rest} />
                        </div>
                      )}
                    </Field>
                  </InputAndLabel>
                  <InputAndLabel>
                    <InputLabel>Linked accounts</InputLabel>
                    <Field name="linked_accounts" options={accountOptions}>
                      {({ input, ...rest }) => (
                        <Select
                          {...input}
                          {...rest}
                          onChange={(selected, removedValue) => {
                            if (selected && input.value) {
                              input.value.push(selected);
                            } else if (input.value) {
                              input.value = input.value.filter(
                                item =>
                                  item.value !== removedValue.removedValue.value
                              );
                            } else if (selected && !input.value) {
                              input.value = [selected];
                            }
                            input.onChange(selected, removedValue);
                          }}
                          isMulti
                        />
                      )}
                    </Field>
                  </InputAndLabel>
                </InputAndLabelRow>
                <GreenButton type="submit" text="NEXT" />
              </InputContainer>
            </AdditionalInfo>
          </Container>
        </form>
      )}
    />
  );
};
export default Profile;
