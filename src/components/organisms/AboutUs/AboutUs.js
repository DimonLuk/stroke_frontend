import React from "react";
import "hover.css";
import "animate.css";
import styled from "styled-components";

function AboutUs() {
  const StrokeSignsContainer = styled.div`
    width: 90%;
    margin: 100px auto 0px;
  `;
  const StrokeSignsHeader = styled.h1`
    margin: 100px 0px 0px 100px;
    font-size: 60px;
    font-weight: 700;
    font-family: "Montserrat", sans-serif;
    color: #333333;

    @media (max-width: 660px) {
      margin: 0px 0px 0px 0px;

      font-size: 50px;
    }

    @media (max-width: 500px) {
      margin: 0px 0px 0px 0px;

      font-size: 45px;
    }

    @media (max-width: 400px) {
      font-size: 40px;
    }
  `;
  const StrokeSignsBigText = styled.p`
    margin: 100px 0px;
    font-size: 40px;
    line-height: 50px;
    font-family: "Montserrat", sans-serif;
    width: 85%;
    font-weight: 500;
    color: #333333;
    @media (max-width: 960px) {
      font-size: 35px;
      line-height: 45px;
    }
    @media (max-width: 660px) {
      font-size: 27px;
      line-height: 40px;
    }

    @media (max-width: 500px) {
      font-size: 22px;
      line-height: 35px;
    }

    @media (max-width: 450px) {
      font-size: 20px;
      line-height: 30px;
    }
  `;
  const StrokeSignsSmallText = styled.p`
    margin: 50px 0px;
    font-size: 20px;
    line-height: 40px;
    font-family: "Montserrat", sans-serif;
    width: 60%;
    font-weight: 500;
    color: #333333;
    @media (max-width: 960px) {
      font-size: 18px;
      line-height: 35px;
    }

    @media (max-width: 660px) {
      font-size: 16px;
      line-height: 30px;
      width: 100%;
    }
  `;
  return (
    <React.Fragment>
      <StrokeSignsContainer>
        <StrokeSignsHeader>About us</StrokeSignsHeader>
        <StrokeSignsBigText>
          We are greeting you in our program. We created the application, which
          must help to define the probability of insult. We aimed our product at
          achieving the success namely rescuing human lives.
        </StrokeSignsBigText>
        <StrokeSignsSmallText>
          The main purpose is preventing strokes. This is possible if user
          implements system`s requirements. He must pass the test, which
          consists of twenty questions, and enter the coagulogram data, which is
          available after examination in any hospital.
        </StrokeSignsSmallText>
        <StrokeSignsSmallText>
          Every question asks about certain sign, which can cause the stroke, so
          the more or the higher particular sign of health, the higher
          probability to have a stroke. It is also possible that the answer on
          special question essentially influences the next one.
        </StrokeSignsSmallText>
        <StrokeSignsSmallText>
          This advantage is not only in our product. Users are enabled to create
          the family account. It allows members to check other members` profiles
          to be aware of their health state. We provide the information which is
          full of symptoms, reasons of stroke causing, diagnostics to
          application and provide preventions and recommendations for our users.
        </StrokeSignsSmallText>
      </StrokeSignsContainer>
    </React.Fragment>
  );
}

export default AboutUs;
