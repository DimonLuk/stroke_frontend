import React from "react";
import green from "@material-ui/core/colors/green";
import styled from "styled-components";
function ErrorPage(props) {
  const Bg = styled.div`
    display: flex;
    margin: 100px auto;
    width: 90%;
  `;
  const TextContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 50%;
  `;
  const ErrorHeader = styled.h1`
    font-family: "Montserrat", sans-serif;
    color: ${green[500]};
    font-size: 50px;
    font-weight: 900;
  `;
  const ErrorBigText = styled(ErrorHeader)`
    font-size: 20px;
    color: #444444;
    margin: 50px 0px 0px;
    font-weight: 500;
    line-height: 70px;
  `;
  return (
    <React.Fragment>
      <Bg>
        <TextContainer>
          <ErrorHeader>This page was lost</ErrorHeader>
          <ErrorBigText>
            The page that you tried to access doesn't exist.
          </ErrorBigText>
        </TextContainer>
        <div />
      </Bg>
    </React.Fragment>
  );
}
export default ErrorPage;
