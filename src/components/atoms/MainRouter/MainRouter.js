import React from "react";
import { Switch, Route } from "react-router-dom";
import Welcome from "../../organisms/Welcome/Welcome";
import AuthForm from "../../organisms/AuthForm/AuthForm";
import StrokeSigns from "../../organisms/StrokeSigns/StrokeSigns";
import ErrorPage from "../../organisms/ErrorPage/ErrorPage";
import AboutUs from "../../organisms/AboutUs/AboutUs";
import Coagulogram from "../../organisms/Coagulogram/Coagulogram";
import StrokeTest from "../../organisms/StrokeTest/StrokeTest";
import Result from "../../organisms/Result/Result";
import Profile from "../../organisms/Profile/Profile";
import AdditionalInfo from "../../organisms/AdditionalInfo/AdditionalInfo";

const MainRouter = props => (
  <main className={props.className}>
    <Switch>
      <Route exact path="/" component={Welcome} />
      <Route path="/login" component={AuthForm} />
      <Route path="/strokesigns" component={StrokeSigns} />
      <Route path="/aboutus" component={AboutUs} />
      <Route path="/coagulogram" component={Coagulogram} />
      <Route path="/test" component={StrokeTest} />
      <Route path="/result" component={Result} />
      <Route path="/profile" component={Profile} />
      <Route path="/additionalinfo" component={AdditionalInfo} />
      <Route component={ErrorPage} />
    </Switch>
  </main>
);

export default MainRouter;
