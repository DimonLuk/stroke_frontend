import React from "react";
import ListItmTxt from "@material-ui/core/ListItemText";

function ListItemText(props) {
  return <ListItmTxt {...props}>{props.children}</ListItmTxt>;
}
export default ListItemText;
