import React from "react";
import Btn from "@material-ui/core/Button";

function Button(props) {
  return (
    <Btn {...props} className={props.className}>
      {props.text}
    </Btn>
  );
}
export default Button;
