import React, { createContext, useState } from "react";

export const IsLoggedContext = createContext();

const IsLoggedContextProvider = props => {
  const [isLogged, setIsLogged] = useState({ isLogged: false, result: "" });
  return (
    <IsLoggedContext.Provider value={[isLogged, setIsLogged]}>
      {props.children}
    </IsLoggedContext.Provider>
  );
};

export default IsLoggedContextProvider;
