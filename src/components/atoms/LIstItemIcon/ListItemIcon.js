import React from "react";
import ListItmIcon from "@material-ui/core/ListItemIcon";

function ListItemIcon(props) {
  return <ListItmIcon {...props}>{props.children}</ListItmIcon>;
}
export default ListItemIcon;
