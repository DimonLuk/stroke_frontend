import React from "react";
import ListItm from "@material-ui/core/ListItem";

function ListItem(props) {
  return <ListItm {...props}>{props.children}</ListItm>;
}
export default ListItem;
