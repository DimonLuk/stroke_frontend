import React from "react";
import Text from "../../atoms/Text/Text";
import { Link as link } from "react-router-dom";
import styled from "styled-components";
import logo from "../../../img/logo.svg";
function Logo() {
  const Link = styled(link)`
    text-decoration: none;
  `;
  const LogoImg = styled.img`
    position: relative;
    right: 20px;
    width: 45px;
  `;
  const LogoTxt = styled(Text)`
    position: relative;
    right: 20px;
    color: white;
    font-family: Roboto;
  `;
  return (
    <React.Fragment>
      <LogoImg src={logo} alt={"logo"} />
      <Link to="/">
        <LogoTxt variant={"h6"} text={"StrokeML"} />
      </Link>
    </React.Fragment>
  );
}
export default Logo;
