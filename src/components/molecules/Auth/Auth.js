import React from "react";
import AccountCircle from "@material-ui/icons/AccountCircle";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import IconButton from "@material-ui/core/IconButton";
import { Link as link } from "react-router-dom";
import styled from "styled-components";
function Auth(props) {
  const { className, open, anchorEl } = props;
  const Link = styled(link)`
    text-decoration: none;
  `;
  return (
    <div className={className}>
      <IconButton
        aria-owns={open ? "menu-appbar" : undefined}
        aria-haspopup="true"
        onClick={props.handleMenu}
        color="inherit"
      >
        <AccountCircle />
      </IconButton>
      <Menu
        id="menu-appbar"
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right"
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right"
        }}
        open={open}
        onClose={props.handleClose}
      >
        <Link to="/profile">
          <MenuItem onClick={props.handleClose}>Profile</MenuItem>
        </Link>
        <Link
          to="/"
          onClick={() => {
            sessionStorage.removeItem("token");
            sessionStorage.removeItem("bearer");
            localStorage.removeItem("username");
            props.handleClose();
          }}
        >
          <MenuItem>Exit</MenuItem>
        </Link>
      </Menu>
    </div>
  );
}
export default Auth;
