import React from "react";
import ListItem from "../ListItem/ListItem";
import ListItemText from "../../atoms/ListItemText/ListItemText";
import list from "../List/List";
import ListItemIcon from "../../atoms/LIstItemIcon/ListItemIcon";
import Divider from "@material-ui/core/Divider";
import MailIcon from "@material-ui/icons/Mail";
import InboxIcon from "@material-ui/icons/Inbox";
import Text from "../../atoms/Text/Text";
import AccountCircle from "@material-ui/icons/AccountCircle";
import Drawer from "@material-ui/core/Drawer";
import styled from "styled-components";
import { Link as link } from "react-router-dom";
import logo from "../../../img/logo.svg";

function MenuDrawer(props) {
  const List = styled(list)`
    color: white;
  `;
  const Link = styled(link)`
    text-decoration: none;
  `;
  const LogoImg = styled.img`
    height: 50px;
  `;
  const { left } = props;

  return (
    <React.Fragment>
      <Drawer open={left} onClose={props.toggleDrawer(false)}>
        <div
          tabIndex={0}
          role="button"
          onClick={props.toggleDrawer(false)}
          onKeyDown={props.toggleDrawer(false)}
        >
          <List
            style={{
              backgroundColor: "#43a047",
              color: "white"
            }}
          >
            <Link to="/">
              <ListItem>
                <ListItemIcon>
                  <LogoImg src={logo} alt={"logo"} />
                </ListItemIcon>
                <ListItemText>
                  <Text
                    variant="h6"
                    color="inherit"
                    text={"StrokeML"}
                    style={{
                      color: "white",
                      position: "relative",
                      right: "15px"
                    }}
                  />
                </ListItemText>
              </ListItem>
            </Link>
          </List>
          <List style={{ paddingLeft: "10px" }}>
            {/* {['About Us', 'Contacts', 'Privacy Policy', 'Bullshit'].map((text, index) => (*/}
            <Link to="/aboutus">
              <ListItem button>
                <ListItemIcon>
                  <InboxIcon />
                </ListItemIcon>
                <ListItemText primary={"About Us"} />
              </ListItem>
            </Link>
            <Link to="/strokesigns">
              <ListItem button>
                <ListItemIcon>
                  <MailIcon />
                </ListItemIcon>
                <ListItemText primary={"Stroke Signs"} />
              </ListItem>
            </Link>
            {localStorage.username && (
                <>
                  <Link to="/coagulogram">
                    <ListItem button>
                      <ListItemIcon>
                        <InboxIcon />
                      </ListItemIcon>
                      <ListItemText primary={"Coagulogram"} />
                    </ListItem>
                  </Link>
                  <Link to="/test">
                    <ListItem button>
                      <ListItemIcon>
                        <MailIcon />
                      </ListItemIcon>
                      <ListItemText primary={"Test"} />
                    </ListItem>
                  </Link>
                </>
              )}
          </List>
          {!localStorage.username && (
            <>
              <Divider />
              <List style={{ paddingLeft: "10px" }}>
                <Link to="/login">
                  <ListItem button>
                    <ListItemIcon>
                      <AccountCircle />
                    </ListItemIcon>
                    <ListItemText primary={"Log in"} />
                  </ListItem>
                </Link>
              </List>
            </>
          )}
        </div>
      </Drawer>
    </React.Fragment>
  );
}
export default MenuDrawer;
