import React from "react";
import Lst from "@material-ui/core/List";

function List(props) {
  return <Lst {...props}>{props.children}</Lst>;
}
export default List;
