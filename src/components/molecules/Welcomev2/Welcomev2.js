import React from "react";
import wave from "react-wavify";
import styled from "styled-components";

function Welcomev2(props) {
  const Wave = styled(wave)`
    position: relative;
    top: 20px;
  `;
  return (
    <Wave
      paused={false}
      options={{
        height: props.height || 40,
        amplitude: 20,
        speed: 0.2,
        points: 4
      }}
    />
  );
}
export default Welcomev2;
