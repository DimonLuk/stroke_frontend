import React from "react";
import "../../../../node_modules/hover.css/css/hover.css";
import "animate.css";
import Text from "../../atoms/Text/Text";
import Button from "../../atoms/Button/Button";
import green from "@material-ui/core/colors/green";
import styled from "styled-components";
import { Link } from "react-router-dom";

function WelcomeTextAndBtns() {
  const TxtAbove = styled(Text)`
    color: white;
    white-space: pre-line;
    font-weight: 500;
    text-align: center;
    font-family: "Montserrat", sans-serif;
    margin: 0.5em;
    @media (max-width: 450px) {
      margin: 0.5em 10px;
    }
  `;
  const TxtAbove2 = styled(TxtAbove)`
    font-weight: 300;
    @media (max-width: 1100px) {
      font-size: 2em;
    }
  `;
  const TxtAbove3 = styled(TxtAbove)`
    font-weight: 100;

    padding: 0px 100px;
    @media (max-width: 1100px) {
      font-size: 1.5em;
    }
    @media (max-width: 700px) {
      padding: 0px 50px;
    }
    @media (max-width: 450px) {
      padding: 0px 10px;
    }
  `;
  const WelcomeBtns = styled.div`
    margin: 0 auto;
    margin-top: 3em;
    text-align: center;
  `;
  const PrimaryButton = styled(Button)`
    z-index: 1000;
    font-family: "Nunito";
    font-weight: 600;
    &:hover {
      background: ${green[400]};
    }
  `;
  const SecondaryButton = styled(Button)`
    background: white;
    color: ${green[500]};
    margin-left: 4em;
    z-index: 1000;
    font-family: "Nunito";
    font-weight: 600;
    @media (max-width: 330px) {
      margin-left: 2em;
    }
    &:hover {
      background: #d5d5e5;
    }
  `;
  return (
    <div>
      <TxtAbove
        variant={"h1"}
        text={"Prevent your stroke"}
        style={{ color: "white" }}
      />
      <TxtAbove2
        variant={"h2"}
        text={"Do you have some doubts in your health?"}
        style={{ color: "white" }}
      />
      <TxtAbove3
        variant={"h6"}
        style={{ color: "white" }}
        text={
          "We provide suitable solution for your problem. Our app helps you to define the probability of you having a stroke."
        }
      />
      {(localStorage.username === "undefined" ||
        localStorage.username === undefined) && (
        <WelcomeBtns>
          <Link to={"/login"} style={{ textDecoration: "none" }}>
            <PrimaryButton
              size={"large"}
              variant="contained"
              color="primary"
              text="Sign in"
            />
          </Link>
          <Link
            to={"/login"}
            style={{
              textDecoration: "none",
              marginLeft: "4em",
              color: green[500]
            }}
          >
            <SecondaryButton
              size={"large"}
              variant="contained"
              text="Sign up"
            />
          </Link>
        </WelcomeBtns>
      )}
    </div>
  );
}

export default WelcomeTextAndBtns;
