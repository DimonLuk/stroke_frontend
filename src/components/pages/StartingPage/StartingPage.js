import React from "react";
import Header from "../../organisms/Header/Header";
import footer from "../../organisms/Footer/Footer";
import MainRouter from "../../atoms/MainRouter/MainRouter";
import IsLoggedContext from "../../atoms/IsLoggedContext/IsLoggedContext";
import styled from "styled-components";
function StartingPage(props) {
  const Container = styled.div`
    display: flex;
    flex-direction: column;
    min-height: 100vh;
    max-width: 1920px;
    background: #fafafa;
    margin: auto;
  `;
  const Footer = styled(footer)`
    flex-shrink: 0;
  `;
  const Router = styled(MainRouter)`
    flex: 1 0 auto;
  `;
  return (
    <IsLoggedContext>
      <Container>
        <Header />
        <Router />
        <Footer />
      </Container>
    </IsLoggedContext>
  );
}
export default StartingPage;
