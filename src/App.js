import React, { Component } from "react";
import "./App.css";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core";
import { red, green } from "@material-ui/core/colors";
import StartingPage from "./components/pages/StartingPage/StartingPage";
import CssBaseline from "@material-ui/core/CssBaseline";
export const deftheme = createMuiTheme({
  typography: {
    useNextVariants: true
  },
  palette: {
    primary: {
      // light: will be calculated from palette.primary.main,
      main: green[600]
      // dark: will be calculated from palette.primary.main,
      // contrastText: will be calculated to contrast with palette.primary.main
    },
    secondary: {
      main: green[800]
      // dark: will be calculated from palette.secondary.main,
    },

    error: {
      main: red[600]
    }
    // error: will use the default color
  }
});
export const theme = {
  ...deftheme,
  overrides: {
    MuiTypography: {
      h1: {
        fontSize: "3rem",
        fontFamily: "Montserrat",
        lineHeight: 1.2
      },
      h2: {
        fontSize: "2rem",
        fontFamily: "Montserrat"
      }
    }
  }
};

class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <StartingPage />
      </MuiThemeProvider>
    );
  }
}

export default App;
