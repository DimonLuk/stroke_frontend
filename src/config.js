import * as Sentry from "@sentry/browser";

Sentry.init({
  dsn: "https://e4f50c1e8c0d4e25b2244c443afb6309@sentry.io/1467990"
});
let _config = {
  API_ROOT_URL: "https://strokeml.ml/__stroke_api__/",
  MODE: "production",
  FACEBOOK_CLIENT_ID: "yrQj0tALICtlmI7QNE5kHK9UwxokKYk8s8zgGtBj",
  GOOGLE_CLIENT_ID:
    "302758551379-ao0gntupilvh28v878otmt7d2472vjpg.apps.googleusercontent.com"
};

export const config = _config;
