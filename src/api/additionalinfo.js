import { config } from "../config";
import { token as genToken } from "./tokenCheck";
let token = genToken(); 
export const AdditionalInfo = async (
  sex,
  country,
  name,
  age,
  telegram,
  linkedAccounts
) => {
  let res = await fetch(`${config.API_ROOT_URL}user_info/`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: token
    },
    body: JSON.stringify({
      sex,
      country,
      age: parseInt(age),
      telegram_account: telegram
    })
  });
  console.log(await res.json());
};
