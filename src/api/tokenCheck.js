let _token = () => {
  if (sessionStorage.getItem("token")) {
    return `Token ${sessionStorage.getItem("token")}`;
  } else {
    return `Bearer ${sessionStorage.getItem("bearer")}`;
  }
};
export const token = _token;
