import { config } from "../config";
export const Login = async (username, password) => {
  let reg = await fetch(`${config.API_ROOT_URL}login/`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      username,
      password
    })
  });
  console.log(reg);
  reg = await reg.json();
  return reg;
};
