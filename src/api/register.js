import { config } from "../config";
export const Register = async (email, username, password) => {
  let reg = await fetch(`${config.API_ROOT_URL}registration/`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      email,
      username,
      password
    })
  });
  if (reg.status < 400) {
    localStorage.setItem("username", username);
    let token = await fetch(`${config.API_ROOT_URL}login/`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        username,
        password
      })
    });
    token = await token.json();
    sessionStorage.setItem("token", token.token);
    return token;
  }
};
