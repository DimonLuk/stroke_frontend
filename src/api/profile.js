import { config } from "../config";
import { token as getToken } from "./tokenCheck";
export const profile = async () => {
  let res = await fetch(
    `${config.API_ROOT_URL}user_info/${localStorage.getItem("username")}/`,
    {
      headers: {
        Authorization: getToken()
      }
    }
  );
  let resjson = await res.json();
  let anotherRes = await fetch(
    `${config.API_ROOT_URL}registration/${localStorage.getItem("username")}/`,
    {
      headers: {
        Authorization: getToken()
      }
    }
  );
  let anotherResJson = await anotherRes.json();
  return { ...resjson, ...anotherResJson };
};
